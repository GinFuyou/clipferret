#!/usr/bin/python3
# -*- coding: utf-8 -*-

import json
import operator
import os
import re
from datetime import datetime
from time import sleep, time
from unicodedata import category as ucategory

import pyperclip
from sqlalchemy import asc, create_engine
from sqlalchemy.orm import sessionmaker

try:
    from appdirs import user_data_dir
except ImportError:
    print('!! appdirs import error')
    config_path = None
else:
    config_path = user_data_dir('clipferret', 'GinFuyou', roaming=True)

from clipferret.basecommand import BaseCommand
from clipferret.models import Base, GameSession, Line

__version__ = "0.7.11-a2"


HIRAGANA_RANGE = (int("3040", 16), int("309F", 16))
KATAKANA_RANGE = (int("30A0", 16), int("30FF", 16))
CJK_IDEOGRAPH_RANGE = (int("4e00", 16), int("9faf", 16))


def in_unirange(char, range_tuple):
    code = ord(char)
    return range_tuple[0] <= code <= range_tuple[1]


class Command(BaseCommand):
    name = 'Clip Ferret'
    version = __version__
    line_formats = {
        'default': '{count: >3}. {f}{0.text}{s.RESET_ALL}',
        'timed': '[{0.strftime}] {f}{0.text}{s.RESET_ALL}' }
    EXIT = 0
    idle_message = "#" \
        " {1} lines: {f}{0.count}{RESET_ALL} elp.: {f}{2}{RESET_ALL} min.," \
        " kana: {f}{3}{RESET_ALL} kanji: {f}{0.count_kanji}{RESET_ALL}"
    db_engine = None
    db_session_class = None
    db_path = None
    db_session = None

    game = None
    ignore_patterns = []

    def __init__(self):
        self.count = 0
        self.title = '<None>'
        self.lastclip = ''
        #self.currentline = None
        self.lines = []
        self.dump_index = 0  # index of last undumped line
        self.start_time = time()

        self.elapsed_time = 0
        self.charcount = 0
        self.config_path = config_path
        self.session_path = None
        self.session_filepath = None
        # take at least 2 lines step before dumping, so they can be deleted
        self.dump_offset = 2

        self.unicode_by_category = {
            'Cc': 0,  #  Other, Control
            'Cf': 0,  #  Other, Format
            'Cn': 0,  #  Other, Not Assigned (no characters in the file have this property)
            'Co': 0,  #  Other, Private Use
            'Cs': 0,  #  Other, Surrogate
            #
            'LC': 0,  #  Letter, Cased
            'Ll': 0,  #  Letter, Lowercase
            'Lm': 0,  #  Letter, Modifier
            'Lo': 0,  #  Letter, Other
            'Lt': 0,  #  Letter, Titlecase
            'Lu': 0,  #  Letter, Uppercase
            #
            'Mc': 0,  #  Mark, Spacing Combining
            'Me': 0,  #  Mark, Enclosing
            'Mn': 0,  #  Mark, Nonspacing
            #
            'Nd': 0,  #  Number, Decimal Digit
            'Nl': 0,  #  Number, Letter
            'No': 0,  #  Number, Other
            #
            'Pc': 0,  #  Punctuation, Connector
            'Pd': 0,  #  Punctuation, Dash
            'Pe': 0,  #  Punctuation, Close
            'Pf': 0,  #  Punctuation, Final quote (may behave like Ps or Pe depending on usage)
            'Pi': 0,  #  Punctuation, Initial quote (may behave like Ps or Pe depending on usage)
            'Po': 0,  #  Punctuation, Other
            'Ps': 0,  #  Punctuation, Open
            #
            'Sc': 0,  #  Symbol, Currency
            'Sk': 0,  #  Symbol, Modifier
            'Sm': 0,  #  Symbol, Math
            'So': 0,  #  Symbol, Other
            #
            'Zl': 0,  #  Separator, Line
            'Zp': 0,  #  Separator, Paragraph
            'Zs': 0,  #  Separator, Space
            }

        self.count_alpha = 0
        self.count_pun = 0
        self.count_kanji = 0
        self.count_hiragana = 0
        self.count_katakana = 0
        self.kanji_dict = {}

    def init_config(self, path):
        config_filepath = os.path.join(path,'conf.json')
        self.vprint('? config path is "{0}"'.format(config_filepath))
        if os.path.isfile(config_filepath):
            conf_dict = json.load(open(config_filepath))
            self.vprint('? {0!r}'.format(conf_dict), v=2)
            self.session_path = conf_dict.get('session_path', None)
            colour = conf_dict.get('colour', None)
            if colour:
                self.args.colour = colour.upper()
            ignore_patterns = conf_dict.get('ignore_patterns', None)
            if ignore_patterns:
                self.vprint('? loaded ignore patterns:', v=2)
                for pattern in ignore_patterns:
                    self.ignore_patterns.append(re.compile(pattern))
                    self.vprint("? "+pattern, v=2, colour="RED")
            self.vprint('? session_path is "{0}"'.format(self.session_path))
        else:
            self.session_path = self.config_path
            conf_dict = {'version': self.version}
            os.makedirs(path, exist_ok=True)
            with open(config_filepath, 'w') as f:
                json.dump(conf_dict, f)

        if self.session_path and not os.path.isdir(self.session_path):
            os.makedirs(self.session_path, exist_ok=True)

    def readclip(self, f=pyperclip.paste):
        return f()

    def analize_unicode(self, line):
        for c in line.text:
            category_key = ucategory(c)
            self.unicode_by_category[category_key] += 1

            if category_key[0] == 'P':
                self.count_pun += 1
            elif category_key in ('Ll', 'Lt', 'Lu'):
                self.count_alpha += 1
            elif category_key == 'Lo':
                if in_unirange(c, HIRAGANA_RANGE):
                    self.count_hiragana += 1
                elif in_unirange(c, KATAKANA_RANGE):
                    self.count_katakana += 1
                elif in_unirange(c, CJK_IDEOGRAPH_RANGE):
                    self.count_kanji += 1
                    if c in self.kanji_dict:
                        self.kanji_dict[c] += 1
                    else:
                        self.kanji_dict[c] = 1

    def printline(self, line, formatkey='default'):
        f = self.get_fore(self.args.colour)
        s = self.line_formats[formatkey].format(line, f=f, s=self.Style, count=self.count)
        last_width = len(line.text.split('\n')[-1])
        if last_width < self.args.width:
            cleanup = "{0: <{1}}\n".format('.', max(self.args.width - last_width, 5))
        else:
            cleanup = '\n'
        self.vprint(s, v=0, end=cleanup)

    def add_arguments(self, parser):
        parser.add_argument(
            'session', metavar='session', type=str, nargs='?',
            help='session name to load')
        parser.add_argument(
            '-c','--colour',type=str, default='BLUE',
            help='colour for line output')
        parser.add_argument('-p', '--poll', type=int, default=200, help='clipboard poll interval in ms')
        parser.add_argument('-w', '--width', type=int, default=80, help='terminal wdth in chars')
        parser.add_argument('-d', '--dumplines', type=int, default=15, help='dump lines step')

    def make_idle_message(self, nocolour=False):
        if nocolour:
            f = ''
            RESET_ALL = ''
        else:
            f = self.get_fore('YELLOW')
            RESET_ALL = self.Style.RESET_ALL
        return self.idle_message.format(
            self,
            datetime.now().strftime('%H:%M:%S'),
            int(self.elapsed_time / 60), 
            (self.count_katakana + self.count_hiragana),
            f=f,
            RESET_ALL=RESET_ALL    )

    def dumplines(self, fullslice=False):
        self.vprint('? dump lines to file', v=2)
        # make a fullslice of lines or set a offset
        if fullslice:
            lineslice = self.lines[self.dump_index:]
        else:
            lineslice = self.lines[self.dump_index:-self.dump_offset]

        if not self.session_filepath:
            return False
        with open(self.session_filepath, 'a') as f:
            for line in lineslice:
                if not line.is_discarded:  # and not line.is_dumped:
                    f.write(line.serialize())
                    f.write('\n')
                    line.is_dumped = True

        # make sure dump_index is set right
        if fullslice:
            self.dump_index = len(self.lines) - 1
        else:
            self.dump_index = len(self.lines) - self.dump_offset

    def check_ignored(self, text):
        yuumu = True
        if self.ignore_patterns:
            for pattern in self.ignore_patterns:
                if pattern.match(text):
                    self.vprint("Ignore: '{0}' @ r'{1}'".format(text, pattern), v=2, colour='RED')
                    yuumu = False
                    break
        return yuumu

    def loop(self):
        loop_timer = time()
        text = self.readclip().strip()
        if text and text != self.lastclip:
            if self.check_ignored(text):
                self.lastclip = text
                line = Line()
                line.text = text
                line.session = self.game
                line.datetime = datetime.now()
                self.db_session.add(line)
                self.db_session.commit()
                self.lines.append(line)
                self.count += 1
                self.printline(line)
                # calc stats
                self.charcount += len(text)
                self.analize_unicode(line)
            else:
                self.lastclip = text

        sleep(self.args.poll/1000)

        # calc time for statistics
        self.elapsed_time += (time() - loop_timer)
        self.vprint(self.make_idle_message(), end='\r', v=0)

    def run(self):
        try:
            while self.command != self.EXIT:
                self.loop()
        except KeyboardInterrupt:
            self.vprint("\nKeyboardInterrupt, paused", v=0)
            kotae = ''
            while kotae not in ('x', 'r'):
                kotae = input('Action: (r) resume (x) exit (w) write (k) kanji (c) colour (d) delete line\n> ')
                kotae = kotae.lower()

                if kotae in ('w', 'x'):
                    self.db_session.commit()
                if kotae == 'x':
                    self.db_session.close()
                    self.vprint('Lines: {0}'.format(self.count), v=0)
                elif kotae == 'k':
                    l = sorted(self.kanji_dict.items(), key=operator.itemgetter(1), reverse=True)  # ? [:40]
                    i = 1
                    for kanji, count in l:
                        self.vprint("{0}: {1: >3}".format(kanji, count), v=0, end='   ')
                        if (i % 4) == 0:
                            self.vprint('', v=0)
                        i += 1
                    self.vprint('', v=0)
                elif kotae == 'c':
                    new_colour = input('Input new colour name: ')
                    new_colour = new_colour.strip().upper()
                    #f = self.get_fore(new_colour)
                    self.vprint("\nNo one expects Spanish inquisition changing colours!", colour=new_colour, v=0)
                    self.args.colour = new_colour

            return kotae
        except Exception as ex:
            self.db_session.commit()
            self.db_session.close()
            raise ex

    def initdb(self):
        self.db_path = os.path.join(self.session_path, "clipferret.sqlite")
        self.db_engine = create_engine('sqlite:///{0}'.format(self.db_path))
        Base.metadata.bind = self.db_engine
        if os.path.isfile(self.db_path):
            self.vprint('* loaded DB: {0}'.format(self.db_path), v=1)
        else:
            self.vprint('* create NEW DB: {0}'.format(self.db_path), v=1)
            Base.metadata.create_all()

        self.db_session_class = sessionmaker(bind=self.db_engine)
        self.db_session = self.db_session_class()

    def handle(self):
        if self.config_path:
            self.init_config(self.config_path)
        self.initdb()

        game_title = ""
        if not self.args.session:
            sessions_query = self.db_session.query(GameSession)
            self.vprint("No session title given, scan DB", v=0)
            counter = 0
            for game_session in sessions_query:
                counter += 1
                self.vprint(f" {counter:>2}. {game_session.title} #{game_session.id}", v=0)

            while len(game_title) < 2:
                game_title = input("input title > ").strip()
        else:
            game_title = self.args.session
            
        game_title = game_title.strip().title()
        
        games_query = self.db_session.query(GameSession).filter(GameSession.title == game_title)
        if games_query.count():
            self.game = games_query.one()
            self.vprint(f"Loading game session: {self.game.title} #{self.game.id}", v=0)
            lines_qs = self.db_session.query(Line).filter(Line.session == self.game).order_by(asc(Line.datetime)).all()
            for line in lines_qs[-10:]:
                self.printline(line, 'timed')
            self.lastclip = lines_qs[-1].text
        else:
            self.game = GameSession(title=game_title)
            self.db_session.add(self.game)
            self.db_session.commit()
            self.vprint("New game session: {0.title} #{0.id}".format(self.game), v=0)


        self.command = 1
        self.vprint("Starting loop", v=1)
        kotae = 'r'
        while kotae != 'x':
            kotae = self.run()
        return self.command


if __name__ == '__main__':
    Command().execute()
