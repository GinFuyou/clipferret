import os
import sys
#import warnings
import argparse
from time import localtime, strftime

from colorama import init as colorama_init
from colorama import Fore, Back, Style


class BaseCommand(object):
    help = ''
    default_args = ''
    version = '0.1a0'
    name = 'BaseCommand'
    args = None
    start_text = '* {0.name} ver.{1.MAGENTA}{0.version}{2.RESET_ALL} kidou! (v={0.args.verbosity})'
    Style = Style

    def get_fore(self, colour='WHITE'):
        return getattr(Fore, colour.upper(), 'WHITE')

    def create_parser(self):
        """
        Create and return the ``ArgumentParser`` which will be used to
        parse the arguments to this command.

        """
        parser = argparse.ArgumentParser(description=self.help or None)
        parser.add_argument('--version', action='version', version=self.version)
        parser.add_argument('-v', '--verbosity', action='count', dest='verbosity', default=0,
            #type=int, # choices=[0, 1, 2, 3],
            help='Verbosity level; 0=minimal output, 1=normal output, 2=verbose output, 3=very verbose output')
        parser.add_argument('--traceback', action='store_true',
            help='Raise on CommandError exceptions')
        parser.add_argument('--no-color', action='store_true', dest='no_color', default=False,
            help="Don't colorize the command output.")

        if self.default_args:
            # Keep compatibility and always accept positional arguments, like optparse when args is set
            parser.add_argument('args', nargs='*')
        self.add_arguments(parser)
        return parser

    def add_arguments(self, parser):
        pass

    def execute(self):
        """
        Try to execute this command, performing system checks if needed (as
        controlled by attributes ``self.requires_system_checks`` and
        ``self.requires_model_validation``, except if force-skipped).
        """
        parser = self.create_parser()
        self.args = parser.parse_args()
        colorama_init()
        start_text = self.start_text.format(self, Fore, Style)
        self.vprint(start_text, v=1)
        self.handle()

    def vprint(self, text, v=2, end='\n' , colour=None):
        if self.args.verbosity >= v:
            if colour:
                f = self.get_fore(colour)
                text = "{0}{1}{2.RESET_ALL}".format(f, text, Style)
            print(text, end=end)

    def handle(self):
        """
        The actual logic of the command. Subclasses must implement
        this method.

        """
        raise NotImplementedError('subclasses of BasePonyCommand must provide a handle() method')
