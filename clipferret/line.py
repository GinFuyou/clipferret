from time import time, sleep
from datetime import datetime



class Line(object):
    timeformat = ('%Y-%m-%d %H:%M:%S')


    def __init__(self, text, n=0):
        self.text = text.strip('#')
        self.dt = datetime.now()
        self.n = n
        self.is_discarded = False
        self.is_dumped = False

        ## TEST
        #t = time()
        #self.analize_unicode()
        #t = time() - t
        #print('? Unicode processing time: {0:.3f}s.'.format(t))

    def serialize(self):
        if self.is_discarded:
            return ''
        else:
            return '[{1}] {0.text}'.format(self, self.dt.strftime(self.timeformat))
