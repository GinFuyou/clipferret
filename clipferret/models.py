# -*- coding: utf-8 -*-

from datetime import datetime as dt
#from dateutil import parser
from sqlalchemy import Column, Integer, String, ForeignKey, DateTime
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base


Base = declarative_base()

class GameSession(Base):
    __tablename__ = 'sessions'
    id = Column(Integer, primary_key=True)
    title = Column(String, unique=True)

class Line(Base):
    __tablename__ = 'lines'

    _timeformat = ('%Y-%m-%d %H:%M:%S')

    id = Column(Integer, primary_key=True)

    text = Column(String)
    datetime = Column(DateTime)
    session_id = Column(Integer, ForeignKey('sessions.id'))
    session = relationship("GameSession", back_populates="lines")

    @property
    def strftime(self):
        return dt.strftime(self.datetime, self._timeformat) 

    def __repr__(self):
        return '[{1}] {0.text}'.format(self, self.strftime)

GameSession.lines = relationship("Line", order_by=Line.id, back_populates="session")
