# -*- coding: utf-8 -*-
from setuptools import setup

packages = \
['clipferret']

package_data = \
{'': ['*']}

install_requires = \
['SQLAlchemy>=1.4.5,<2.0.0',
 'appdirs>=1.4.4,<2.0.0',
 'colorama>=0.4.4,<0.5.0',
 'pyperclip>=1.8.2,<2.0.0']

setup_kwargs = {
    'name': 'clipferret',
    'version': '0.7.11a2',
    'description': '',
    'long_description': None,
    'author': 'Gin Fuyou',
    'author_email': 'webmaster@doratoa.net',
    'maintainer': None,
    'maintainer_email': None,
    'url': None,
    'packages': packages,
    'package_data': package_data,
    'install_requires': install_requires,
    'python_requires': '>=3.6.5,<4.0.0',
}


setup(**setup_kwargs)
